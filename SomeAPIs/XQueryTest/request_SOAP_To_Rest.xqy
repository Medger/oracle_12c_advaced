xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare default element namespace "";
(:: import schema at "request.xsd" ::)
declare namespace ns1="http://TargetNamespace.com/REST_API_FROM_SOA_Operation1_request";
(:: import schema at "Resources/request_soa_rest.xsd" ::)

declare variable $request as element() (:: schema-element(XqueryRequest) ::) external;

declare function local:func($request as element() (:: schema-element(XqueryRequest) ::)) as element() (:: schema-element(ns1:Root-Element) ::) {
    <ns1:Root-Element>
        <ns1:number>{fn:data($request/pid)}</ns1:number>
        <ns1:desOfName>{fn:data($request/idName)}</ns1:desOfName>
        <ns1:People>
            <ns1:name>{fn:data($request/ListOfData/firstName)}</ns1:name>
            <ns1:lastName>{fn:data($request/ListOfData/lastName)}</ns1:lastName>
        </ns1:People>
    </ns1:Root-Element>
};

local:func($request)
