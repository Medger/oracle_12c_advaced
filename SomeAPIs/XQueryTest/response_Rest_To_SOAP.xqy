xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare default element namespace "";
(:: import schema at "response.xsd" ::)
declare namespace ns1="http://TargetNamespace.com/REST_API_FROM_SOA_Operation1_response";
(:: import schema at "Resources/response_soa_rest.xsd" ::)

declare variable $response as element() (:: schema-element(ns1:Root-Element) ::) external;

declare function local:func($response as element() (:: schema-element(ns1:Root-Element) ::)) as element() (:: schema-element(XqueryResponse) ::) {
    <XqueryResponse>
        <ListOfData>
            <firstName>{fn:data($response/ns1:data/ns1:name)}</firstName>
            <lastName>{fn:data($response/ns1:data/ns1:lastName)}</lastName>
            <idName>{fn:data($response/ns1:desc)}</idName>
            <pid>{fn:data($response/ns1:id)}</pid>
        </ListOfData>
    </XqueryResponse>
};

local:func($response)
